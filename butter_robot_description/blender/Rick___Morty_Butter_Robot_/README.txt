                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1386866
Rick & Morty Butter Robot  by jimbone is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

UPDATE: Been meaning to fix parts of this print, Everything prints with out supports except the wheels and just snip off the extra plastic. I turned the wheels around to get a nice finish on the front and the head I made it so the lens can be screwed on since I noticed different slicers made it hard for the old lens to fit in. new files are marked and enjoy. The light can be added easily with a blinking led and battery just check your local electronics store or ebay. 
What is my purpose!
short video of the blinking light I added, was previously a bike helmet light. https://youtu.be/Np1iKPkuH44 
I couldn't find a Model that was in pieces so I made my own. 
Fully movable Butter Robot
The acrylic lens was a idea that you could paint the inside then add resin or acrylic just enough to make a clear bubble.
I designed his eye around a lens I had found but had made a copy of it to paint. 
All the holes are designed around M3 screws but I don't know the length of all as I had just used what I had in my loose screws jar.
2 versions of the head, one is hollow and a hole to the LED to add a blinking light as well as a possible voice box. 
For the wires I recommend just finding some thick wire to cut and fit other wise I will post a printable wire. 
The track is held in place with a lip that the wheels catch onto. 


# Print Settings

Printer Brand: RepRap
Printer: Prusa i3 Rework
Rafts: No
Supports: Yes
Resolution: .2 layer 
Infill: 10-20

Notes: 
updated files can be printed without support except for the new wheels.